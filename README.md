README:

How to Get Started:
    1. while in the snake-game directory, create a virtual enviroment with the command 'python -m venv .venv'
    2. launch the virtual enviroment with the command './.venv/Scripts/Activate.ps1'
    3. install requirements with the command 'pip install -r requirements.txt'
    4. run the main.py file

Instructions:
    -Use the either the "WASD" keys or the arrow keys to change direction of the miner
    -Collect jems to score points
    -Dont crash into the minecarts or the edge of the screen
    -Have Fun!!!