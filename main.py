import pygame
import os
import random
from pygame import mixer

# initializes pygames font actions
pygame.font.init()
pygame.mixer.init()

# defining constant variables
WIDTH, HEIGHT = 900, 900
# defines the window
WIN = pygame.display.set_mode((WIDTH, HEIGHT))
GREY = (128, 128, 128)
BLACK = (47, 79, 79)
FPS = 12
MINER_HEIGHT, MINER_WIDTH = 50, 50
MINECART_HEIGHT, MINECART_WIDTH = 45, 45
JEWEL_HEIGHT, JEWEL_WIDTH = 50, 50
# defines the fonts
SCORE_FONT = pygame.font.SysFont('arialblack', 28)
HIGHSCORE_FONT = pygame.font.SysFont('arialblack', 16)
GAMEOVER_FONT = pygame.font.SysFont('arialblack', 40)
# defines the collision box for the border
BORDER_COLLISION_BOX = pygame.Rect(MINER_WIDTH - 1, MINER_HEIGHT - 1, WIDTH - 2*MINER_WIDTH + 2, HEIGHT - 2*MINER_HEIGHT + 2)
# Creating Custom Events
JEWEL_COLLECTED = pygame.USEREVENT + 1
MINER_CRASH = pygame.USEREVENT + 2
# imports the image from assets. (os.path.join makes the path work on both mac and windows)
MINER_IMAGE = pygame.image.load(os.path.join('Assets', 'Dwarf.png'))
# Resizes the image
MINER_IMAGE = pygame.transform.scale(MINER_IMAGE, (50, 50))
# more images
JEWEL_IMAGE = pygame.image.load(os.path.join('Assets', 'jewelscaled.png'))
JEWEL_IMAGE = pygame.transform.scale(JEWEL_IMAGE, (45, 45))
MINECART_IMAGE = pygame.image.load(os.path.join('Assets', 'minecartwithjewel.png'))
MINECART_IMAGE = pygame.transform.scale(MINECART_IMAGE, (45, 45))
JEWEL_COLLECTED_SOUND = pygame.mixer.Sound(os.path.join('Assets', 'gem-sound.wav'))
MINER_CRASH_SOUND = pygame.mixer.Sound(os.path.join('Assets', 'crash-sound.wav'))

# sets the name of the window
pygame.display.set_caption("Miners")

# adds music
pygame.mixer.music.load(os.path.join('Assets', 'Dwarven-Mines.wav'))
pygame.mixer.music.set_volume(0.1)
pygame.mixer.music.play()


# defining functions for the game
def draw_window(miner, jewel, score, minecarts, highscore):
        # makes the window background color grey
        WIN.fill(GREY)
        # displays miner on the window using the miner rectangles size and position
        WIN.blit(MINER_IMAGE, (miner.x, miner.y))
        # displays the jewel
        WIN.blit(JEWEL_IMAGE, (jewel.x, jewel.y))
        # # draws minecarts
        for minecart in minecarts:
            WIN.blit(MINECART_IMAGE, (minecart.x, minecart.y))
        # draws score
        score_text = SCORE_FONT.render("Score: " + str(score), 1, BLACK)
        WIN.blit(score_text, (725, 25))
        # draws highscore
        highscore_text = HIGHSCORE_FONT.render("Highscore: " + str(highscore), 1, BLACK)
        WIN.blit(highscore_text, (727, 55))
        # updates the display
        pygame.display.update()

# sets the direction the miner is moving in
def miner_direction(keys_pressed, direction):
    if (keys_pressed[pygame.K_a] or keys_pressed[pygame.K_LEFT]) and direction != "right":
        direction = "left"
    if (keys_pressed[pygame.K_d] or keys_pressed[pygame.K_RIGHT]) and direction != "left":
        direction = "right"
    if (keys_pressed[pygame.K_w] or keys_pressed[pygame.K_UP]) and direction != "down":
        direction = "up"
    if (keys_pressed[pygame.K_s] or keys_pressed[pygame.K_DOWN]) and direction != "up":
        direction = "down"
    return direction

# moves the miner width of a minecart + 5 in current direction with the screen border as bounds
def miner_movement(direction, miner):
    if direction == "left" and miner.x >= -1:
        miner.x -= MINECART_WIDTH + 5
    if direction == "right" and miner.x <= WIDTH - MINER_WIDTH + 1:
        miner.x += MINECART_WIDTH + 5
    if direction == "up" and miner.y >= -1:
        miner.y -= MINECART_WIDTH + 5
    if direction == "down" and miner.y <= HEIGHT - MINER_HEIGHT + 1:
        miner.y += MINECART_WIDTH + 5
    return miner


def miner_jewel_collision(miner, jewel, score):
    if miner.colliderect(jewel):
        # moves the jewel to a random location
        jewel.x = random.randrange(0, 900, 50)
        jewel.y = random.randrange(0, 900, 50)
        # posts the jewel_collected event
        pygame.event.post(pygame.event.Event(JEWEL_COLLECTED))
    return score, jewel

def miner_crash(miner, BORDER_COLLISION_BOX, minecarts):
    if not miner.colliderect(BORDER_COLLISION_BOX):
        pygame.event.post(pygame.event.Event(MINER_CRASH))
    for minecart in minecarts:
        if miner.colliderect(minecart):
            pygame.event.post(pygame.event.Event(MINER_CRASH))

def draw_gameover(text):
    # draws the gameover text
    draw_text = GAMEOVER_FONT.render(text, 1, BLACK)
    WIN.blit(draw_text, (WIDTH//2 - draw_text.get_width()//2, HEIGHT//2 - draw_text.get_height()//2))
    pygame.display.update()
    # pauses the game for 4 seconds
    pygame.time.delay(4000)

def minecart_create(minecarts, length):
    while len(minecarts) != length:
        minecarts.append(pygame.Rect(-50, -50, MINECART_WIDTH, MINECART_HEIGHT))
    return minecarts

def minecart_movement(minecarts, miner):
    # moves the x and y of the last minecart to the previous minecart
    if len(minecarts) > 1:
        for i in range(len(minecarts) -1, 0, -1):
            minecart = minecarts[i]
            previous_minecart = minecarts[i - 1]

            minecart.x = previous_minecart.x
            minecart.y = previous_minecart.y

            minecarts[i] = minecart
        # moves the first minecart to the miners position
        first_minecart = minecarts[0]
        first_minecart.x = miner.x
        first_minecart.y = miner.y
        minecarts[0] = first_minecart
        return minecarts

    elif len(minecarts) == 1:
        first_minecart = minecarts[0]
        first_minecart.x = miner.x
        first_minecart.y = miner.y
        minecarts[0] = first_minecart
        return minecarts

    else:
        pass
        
        
# highscore variable outside of the main loop
highscore = 0
# the function for the main loop that keeps the game running and open
def main(highscore):
    # sets an initial rectangle poisition and size around the following objects
    miner = pygame.Rect(450, 450, MINER_WIDTH, MINER_HEIGHT)
    jewel = pygame.Rect(800, 100, JEWEL_WIDTH - 10, JEWEL_HEIGHT - 10)

    
    # defining variables
    score = 0
    direction = ""
    length = 0
    clock = pygame.time.Clock()
    run = True
    gameover_text = ""
    minecarts = []
    # main loop
    while run:
        # sets the fps
        clock.tick(FPS)
        # checks all the events in pygame
        for event in pygame.event.get():
            # stops main loop if we quit the window
            if event.type == pygame.QUIT:
                run = False
                pygame.quit()

            # increases the score when collecting a jewel
            if event.type == JEWEL_COLLECTED:
                score += 10
                length += 1
                JEWEL_COLLECTED_SOUND.play()

            # handles the gameover conditions
            if event.type == MINER_CRASH:
                gameover_text = "Game Over!  Score: " + str(score)
                if score > highscore:
                    highscore = score
                MINER_CRASH_SOUND.set_volume(0.2)
                MINER_CRASH_SOUND.play()

        # gets keys pressed for the miner direction function
        keys_pressed = pygame.key.get_pressed()
        direction = miner_direction(keys_pressed, direction)
        
        if gameover_text != "":
            draw_gameover(gameover_text)
            # breaks out of the while run loop running the next function which is main()
            break

        miner_jewel_collision(miner, jewel, score)

        minecarts = minecart_create(minecarts, length)

        minecart_movement(minecarts, miner)
        
        miner_movement(direction, miner)

        miner_crash(miner, BORDER_COLLISION_BOX, minecarts)

        draw_window(miner, jewel, score, minecarts, highscore)

        # slows down the speed of the game
        # pygame.time.delay(100)        
    
    main(highscore) 

# makes the main function only run if run this file but not if we import this file to other files
if __name__ == "__main__":
    main(highscore)
